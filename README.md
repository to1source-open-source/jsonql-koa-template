# jsonql-template

This is intend to have all the basic setup for you. And you can start developing your micro service with Koa + json:ql in no time.

## Installation

Clone this project to a different name

```sh
$ git clone git@gitlab.com/to1source-open-source/jsonql-template.git yourProject
```

Then change the git origin to yours

```sh
$ git remote set-url origin git@gitlab.com/your-name/your-repo.git
```

Start coding :)

## Example

This is what the start up looks like, in most case, you don't really want to change it.
Unless you have extra things you want to add (such as CSRF) for browser / server project

```js
const Koa = require('koa');
const jsonql = require('jsonql-koa');
const bodyparser = require('koa-bodyparser');
const config = require('config');

const app = module.exports = new Koa();
const jsonqlSetup = {
  // put your config here if any
};
app.use(bodyparser());
app.use(jsonql(jsonqlSetup));

app.use(async (ctx, next) => {
  ctx.body = 'hello world!';
});

if (!module.parent) {
  app.listen(config.get('port'));
}
```

Then in just start creating resolvers

## TODO

- integrate the contract-cli
- Add cli interface to add resolvers (and trigger contract-cli to update contract)
- Add option to add pre-built solution for different type of projects

---

MIT

to1source CN (c) 2018 AND NEWBRAN LTD (c) 2018
