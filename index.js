/////////////////////////////
//   json:ql Koa Template  //
/////////////////////////////
const Koa = require('koa');
const jsonql = require('jsonql-koa');
const bodyparser = require('koa-bodyparser');
// const session = require('koa-session');
// const CSRF = require('koa-csrf');
const config = require('config');

const app = module.exports = new Koa();

/*
app.keys = [
  config.get('sessionKey'),
  config.get('csrfKey')
];
*/
// app.use(session(app));

app.use(bodyparser());
// @BUG this is the problem we need to setup the csrf on the client
// app.use(new CSRF());

app.use(jsonql());

app.use(async (ctx, next) => {
  ctx.body = 'hello world!';
});

if (!module.parent) {
  app.listen(config.get('port'));
}
