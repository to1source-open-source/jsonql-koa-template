'use strict';
const test = require('ava');
const superkoa = require('superkoa');
const app = require('../index');

const type = 'application/vnd.api+json';
const headers = {
  'Accept': type,
  'Content-Type': type
};
// run
test("Hello world!", async t => {
  const res = await superkoa(app)
    .post('/jsonql')
    .set(headers)
    .send({
      helloWorld: {
        args: {}
      }
    });
  t.is('Hello world!', res.body.data);
});
